# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Instalação ###


### Configuracao ###

**App.js**

Adiciona a seguinte configuracao

```
#!javascript

var configTemplate = {
                tema: 'layoutTema2',
                templateLogin: 'layoutTema2Login',
                htmlLogin: 'loginTema2.html',
            }
            var content = 'content@layoutTema2';
```


Na index.html carregue os segintes scrpts

```
#!html

 
  <!-- Arquivos do tema 02-->
  <link href="/vendor/tema02/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/vendor/tema02/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="/vendor/tema02/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="/vendor/tema02/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/vendor/tema02/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/vendor/tema02/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">

 
 
     <!-- jQuery Library -->
    <script type="text/javascript" src="/vendor/tema02/js/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="/vendor/tema02/js/materialize.js"></script>
    <!--prism-->
    <script type="text/javascript" src="/vendor/tema02/js/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="/vendor/tema02/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    
    <!-- chartjs -->
    <script type="text/javascript" src="/vendor/tema02/js/plugins/chartjs/chart.min.js"></script>
    <script type="text/javascript" src="/vendor/tema02/js/plugins/chartjs/chart-script.js"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="/vendor/tema02/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="/vendor/tema02/js/plugins/sparkline/sparkline-script.js"></script>

    <!-- chartist -->
    <script type="text/javascript" src="/vendor/tema02/js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="/vendor/tema02/js/plugins.js"></script>

```